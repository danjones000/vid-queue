<?php

namespace App\Providers;

use App\Services\ProcessInput;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Queue\Factory as QueueFactoryContract;
use App\Queue\DatabaseConnector;
use App\Services\Trakt;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app[QueueFactoryContract::class]->extend('database', fn () => new DatabaseConnector($this->app['db']));

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->instance(ProcessInput::class, new ProcessInput());
        $this->app->instance(Trakt::class, new Trakt());
    }
}
