<?php

namespace App\Data;

use Spatie\LaravelData\Data as BaseData;
use Spatie\LaravelData\Support\DataConfig;
use Spatie\LaravelData\Support\DataProperty;
use Spatie\LaravelData\DataPipeline;
use Spatie\LaravelData\DataPipes\AuthorizedDataPipe;
use Spatie\LaravelData\DataPipes\CastPropertiesDataPipe;
use Spatie\LaravelData\DataPipes\MapPropertiesDataPipe;
use Spatie\LaravelData\DataPipes\ValidatePropertiesDataPipe;

abstract class Data extends BaseData
{
    public static function fillPipeline(): DataPipeline
    {
        return DataPipeline::create()
            ->into(static::class)
            ->through(AuthorizedDataPipe::class)
            ->through(MapPropertiesDataPipe::class)
            ->through(ValidatePropertiesDataPipe::class)
            ->through(CastPropertiesDataPipe::class);
    }

    public function fill(iterable $payload): static
    {
        $pipeline = static::fillPipeline();

        foreach (static::normalizers() as $normalizer) {
            $pipeline->normalizer($normalizer);
        }

        $properties = $pipeline->using($payload)->execute();
        $dataClass  = app(DataConfig::class)->getDataClass(static::class);

        $dataClass
            ->properties
            ->filter(
                fn (DataProperty $property) => $properties->has($property->name)
            )
            ->each(
                fn (DataProperty $property) => $this->{$property->name} = $properties->get($property->name)
            );

        return $this;
    }
}
