<?php

namespace App\Data;

use App\Exceptions\Quit;
use Spatie\LaravelData\Attributes\DataCollectionOf;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\DataPipeline;

class WatchData extends Data
{
    #[DataCollectionOf(WatchExport::class)]
    public DataCollection $files;
    public array $rawData = [];
    public array $structuredData = [];

    public static function pipeline(): DataPipeline
    {
        return parent::pipeline()
            ->through(DataPipes\ReadExportFiles::class)
            ->through(DataPipes\StructureData::class)
            ;
    }
}
