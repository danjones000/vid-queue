<?php

namespace App\Data\Casts;

use App\Exceptions\Quit;
use Spatie\LaravelData\Casts\Cast;
use Spatie\LaravelData\Support\DataProperty;

class File implements Cast
{
    public function cast(DataProperty $property, mixed $file, array $context): mixed
    {
        if (!file_exists($file)) {
            throw new Quit("$file is not a valid file");
        }

        return realpath($file);
    }
}
