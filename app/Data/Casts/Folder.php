<?php

namespace App\Data\Casts;

use App\Exceptions\Quit;
use Spatie\LaravelData\Casts\Cast;
use Spatie\LaravelData\Support\DataProperty;

class Folder implements Cast
{
    public function cast(DataProperty $property, mixed $file, array $context): mixed
    {
        if (!is_dir($file)) {
            throw new Quit("$file is not a valid folder");
        }

        return realpath($file);
    }
}
