<?php

declare(strict_types=1);

namespace App\Data;

use Illuminate\Support\Carbon;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\DataPipeline;

class WatchFile extends Data
{
    #[WithCast(Casts\File::class)]
    public string $path;
    public ?string $output = null;
    public ?string $showNfo = null;
    public ?string $showTmdb = null;
    public ?string $showImdb = null;
    public ?string $showTitle = null;
    public ?string $movieTmdb = null;
    public ?string $movieImdb = null;
    public int $movieYear = 1900;
    public ?string $epNfo = null;
    public int $season = 0;
    public int $episode = 0;
    public Carbon $watched;
    public Enums\Type $type = Enums\Type::Unknown;

    public static function fromPath(string $path): static
    {
        return static::from(['path' => $path]);
    }

    public static function pipeline(): DataPipeline
    {
        return parent::pipeline()
            ->through(DataPipes\GetShowTmdb::class)
            ->through(DataPipes\GetSeasonEp::class)
            ->through(DataPipes\ParseWatchFile::class)
            ;
    }
}
