<?php

namespace App\Data;

use Spatie\LaravelData\DataPipeline;

class WatchExport extends FileData
{
    public array $rawData = [];

    public static function pipeline(): DataPipeline
    {
        return parent::pipeline()
            ->through(DataPipes\ReadExportFile::class)
            ;
    }
}
