<?php

namespace App\Data\DataPipes;

use App\Data\FileData;
use Illuminate\Support\Collection;
use Spatie\LaravelData\DataPipes\DataPipe;
use Spatie\LaravelData\Lazy;
use Spatie\LaravelData\Optional;
use Spatie\LaravelData\Support\DataClass;
use Spatie\LaravelData\Support\DataConfig;
use Spatie\LaravelData\Support\DataProperty;

class ReadExportFile implements DataPipe
{
    public function handle(mixed $payload, DataClass $class, Collection $properties): Collection
    {
        if (!$properties->has('path')) {
            return $properties;
        }

        $properties['rawData'] = json_decode(file_get_contents($properties->get('path')), true) ?? [];

        return $properties;
    }
}
