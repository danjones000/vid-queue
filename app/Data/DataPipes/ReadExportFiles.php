<?php

namespace App\Data\DataPipes;

use App\Data\FileData;
use App\Data\WatchExport;
use Illuminate\Support\Collection;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\DataPipes\DataPipe;
use Spatie\LaravelData\Lazy;
use Spatie\LaravelData\Optional;
use Spatie\LaravelData\Support\DataClass;
use Spatie\LaravelData\Support\DataConfig;
use Spatie\LaravelData\Support\DataProperty;

class ReadExportFiles implements DataPipe
{
    public function handle(mixed $payload, DataClass $class, Collection $properties): Collection
    {
        if (!$properties->has('files')) {
            $properties['files'] = new DataCollection(WatchExport::class, []);
        }

        if (!$properties->get('files')->count()) {
            return $properties;
        }

        $data = [];
        foreach ($properties->get('files') as $file)
        {
            $data[] = $file->rawData;
        }
        $properties['rawData'] = $data;

        return $properties;
    }
}
