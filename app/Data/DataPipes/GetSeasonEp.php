<?php

declare(strict_types=1);

namespace App\Data\DataPipes;

use App\Data\Enums\Type;
use FFMpeg\FFMpeg;
use Illuminate\Support\Collection;
use SimpleXMLElement;
use Spatie\LaravelData\DataPipes\DataPipe;
use Spatie\LaravelData\Support\DataClass;

use function file_exists;
use function pathinfo;
use function simplexml_load_file;


class GetSeasonEp implements DataPipe
{
    public function handle(mixed $payload, DataClass $class, Collection $properties): Collection
    {
        $path = $properties->get('path');
        $pi   = pathinfo($path);

        $nfo = "{$pi['dirname']}/{$pi['filename']}.nfo";
        if (file_exists($nfo)) {
            $properties['epNfo'] = $nfo;
            $xml = simplexml_load_file($nfo);
            $type = $xml->getName();

            if ($type === 'episodedetails') {
                return $this->getEpFromNfo($properties, $xml);
            } else if ($type === 'movie') {
                return $this->getFilmFromNfo($properties, $xml);
            }
        }

        return $this->getEpFromFfprobe($properties);
    }

    protected function getFilmFromNfo(Collection $properties, SimpleXMLElement $xml): Collection
    {
        $properties['type'] = Type::Movie;
        $properties['showTitle'] = $this->getProp($xml, 'title');
        $properties['movieTmdb'] = $this->getProp($xml, 'tmdbid');
        $properties['movieImdb'] = $this->getProp($xml, 'imdbid');
        if (!is_null($year = $this->getProp($xml, 'year'))) {
            $properties['movieYear'] = (int) $year;
        }

        return $properties;
    }

    protected function getEpFromNfo(Collection $properties, SimpleXMLElement $xml): Collection
    {
        if (!is_null($seas = $this->getProp($xml, 'season'))) {
            $properties['season'] = (int) $seas;
        }
        if (!is_null($ep = $this->getProp($xml, 'episode'))) {
            $properties['episode'] = (int) $ep;
        }

        return $properties;
    }

    protected function getProp(SimpleXMLElement $xml, string $prop): ?string
    {
        return $xml->$prop ? (string) $xml->$prop : null;
    }

    protected function getEpFromFfprobe(Collection $properties): Collection
    {
        $ffmpeg  = FFMpeg::create();
        $ffprobe = $ffmpeg->getFFProbe();
        $tags    = $ffprobe->format($properties->get('path'))->get('tags');

        if ($season  = $tags['season_number'] ?? $tags['SEASON_NUMBER'] ?? null) {
            $properties['season'] = (int) $season;
        }

        if ($episode = $tags['episode_sort'] ?? $tags['EPISODE_SORT'] ?? null) {
            $properties['episode'] = (int) $episode;
        }

        return $properties;
    }
}
