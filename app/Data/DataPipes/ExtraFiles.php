<?php

namespace App\Data\DataPipes;

use App\Data\FileData;
use Illuminate\Support\Collection;
use Spatie\LaravelData\DataPipes\DataPipe;
use Spatie\LaravelData\Lazy;
use Spatie\LaravelData\Optional;
use Spatie\LaravelData\Support\DataClass;
use Spatie\LaravelData\Support\DataConfig;
use Spatie\LaravelData\Support\DataProperty;

class ExtraFiles implements DataPipe
{
    public function handle(mixed $payload, DataClass $class, Collection $properties): Collection
    {
        if (!$properties->has('files') || !$properties->get('files')->count()) {
            return $properties;
        }

        $files = $properties->get('files');
        $col = $files->toCollection();
        foreach ($files->items() as $file) {
            $pi = pathinfo($file->path);
            foreach (glob($pi['dirname'].'/'.$pi['filename'].'*') as $path) {
                if (!$col->first(fn (FileData $data) => $data->path === $path)) {
                    $files[] = $path;
                }
            }
        }
        $properties['files'] = $files;

        return $properties;
    }
}
