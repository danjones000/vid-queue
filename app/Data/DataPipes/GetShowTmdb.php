<?php

declare(strict_types=1);

namespace App\Data\DataPipes;

use App\Data\Enums\Type;
use Illuminate\Support\Collection;
use SimpleXMLElement;
use Spatie\LaravelData\DataPipes\DataPipe;
use Spatie\LaravelData\Support\DataClass;

use function dirname;
use function file_exists;
use function simplexml_load_file;

class GetShowTmdb implements DataPipe
{
    public function handle(mixed $payload, DataClass $class, Collection $properties): Collection
    {
        $path = $properties->get('path');

        $dir = dirname($path);
        do {
            $nfo = "$dir/tvshow.nfo";
            $dir = dirname($dir);
        } while (!file_exists($nfo) && $dir !== '/');

        if (!file_exists($nfo)) {
            return $properties;
        }

        $properties['showNfo'] = $nfo;
        $properties['type'] = Type::Episode;

        $xml = simplexml_load_file($nfo);
        $properties['showTmdb'] = $this->getProp($xml, 'tmdbid');
        $properties['showImdb'] = $this->getProp($xml, 'imdb_id');
        $properties['showTitle'] = $this->getProp($xml, 'title');

        return $properties;
    }

    protected function getProp(SimpleXMLElement $xml, string $prop): ?string
    {
        return $xml->$prop ? (string) $xml->$prop : null;
    }
}
