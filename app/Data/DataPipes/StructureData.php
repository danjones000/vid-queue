<?php

declare(strict_types=1);

namespace App\Data\DataPipes;

use App\Data\Enums\Type;
use App\Data\FileData;
use Illuminate\Support\Collection;
use Spatie\LaravelData\DataPipes\DataPipe;
use Spatie\LaravelData\Lazy;
use Spatie\LaravelData\Optional;
use Spatie\LaravelData\Support\DataClass;
use Spatie\LaravelData\Support\DataConfig;
use Spatie\LaravelData\Support\DataProperty;

class StructureData implements DataPipe
{
    public function handle(mixed $payload, DataClass $class, Collection $properties): Collection
    {
        if (!$properties->has('rawData') || !count($properties->get('rawData'))) {
            return $properties;
        }

        $data = [];
        $films = [];
        foreach ($properties->get('rawData') as $watch) {
            switch ($watch['type']) {
            case (Type::Episode->value):
                $data = $this->getEp($data, $watch);
                break;
            case (Type::Movie->value):
                $films[] = $this->getFilm($watch);
                break;
            }
        }

        $flat = [];
        foreach ($data as $show) {
            $show['seasons'] = array_values($show['seasons']);
            $flat[] = $show;
        }
        $properties['structuredData'] = ['shows' => $flat, 'movies' => $films];

        return $properties;
    }

    protected function getEp(array $data, array $watch): array
    {
        $showTmdb = $watch['showTmdb'];
        $season   = $watch['season'];
        $episode  = $watch['episode'];
        $watched  = $watch['watched'];
        if (!array_key_exists($showTmdb, $data)) {
            $data[$showTmdb] = [];
            $data[$showTmdb]['ids'] = ['tmdb' => $showTmdb];
            $data[$showTmdb]['seasons'] = [];
        }

        if (!array_key_exists($season, $data[$showTmdb]['seasons'])) {
            $data[$showTmdb]['seasons'][$season] = [];
            $data[$showTmdb]['seasons'][$season]['number'] = $season;
            $data[$showTmdb]['seasons'][$season]['episodes'] = [];
        }

        $data[$showTmdb]['seasons'][$season]['episodes'][] = [
            'number'     => $episode,
            'watched_at' => $watched,
        ];

        return $data;
    }

    protected function getFilm(array $watch): array
    {
        $filmTmdb = $watch['movieTmdb'];
        $filmYear = $watch['movieYear'];
        $title    = $watch['showTitle'];
        $watched  = $watch['watched'];

        return [
            'watched_at' => $watched,
            'title'      => $title,
            'year'       => $filmYear,
            'ids'        => [
                'tmdb' => $filmTmdb,
            ],
        ];
    }
}
