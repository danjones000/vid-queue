<?php

declare(strict_types=1);

namespace App\Data\DataPipes;

use App\Data\Enums\Type;
use App\Data\FileData;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Spatie\LaravelData\DataPipes\DataPipe;
use Spatie\LaravelData\Lazy;
use Spatie\LaravelData\Optional;
use Spatie\LaravelData\Support\DataClass;
use Spatie\LaravelData\Support\DataConfig;
use Spatie\LaravelData\Support\DataProperty;
use Illuminate\Support\Str;

class ParseWatchFile implements DataPipe
{
    public function handle(mixed $payload, DataClass $class, Collection $properties): Collection
    {
        $properties['watched'] = Carbon::now();
        $properties['output'] = match ($properties['type']) {
            Type::Movie => sprintf(
                '%s-%d.json',
                Str::slug($properties['showTitle'] ?? uniqid()),
                $properties['movieYear'],
            ),
            default => sprintf(
                '%s-%dx%02d.json',
                Str::slug($properties['showTitle'] ?? uniqid()),
                $properties['season'],
                $properties['episode']
            ),
        };

        return $properties;
    }
}
