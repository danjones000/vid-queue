<?php

namespace App\Data;

use Spatie\LaravelData\Attributes\WithCast;

class FileData extends Data
{
    #[WithCast(Casts\File::class)]
    public string $path;

    public static function fromString(string $path): static
    {
        return static::from(['path' => $path]);
    }
}
