<?php

declare(strict_types=1);

namespace App\Data\Enums;

enum Type: string {
    case Unknown = "unknown";
    case Episode = "episode";
    case Movie = "movie";
}
