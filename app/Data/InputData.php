<?php

namespace App\Data;

use App\Exceptions\Quit;
use Spatie\LaravelData\Attributes\DataCollectionOf;
use Spatie\LaravelData\Attributes\MapInputName;
use Spatie\LaravelData\DataCollection;
use Spatie\LaravelData\Attributes\WithCast;
use Spatie\LaravelData\DataPipeline;

class InputData extends Data
{
    #[DataCollectionOf(FileData::class)]
    #[MapInputName('input')]
    public DataCollection $files;
    public ?string $url = null;
    #[WithCast(Casts\Folder::class)]
    public ?string $destination = null;
    public ?string $title = null;
    public bool $sync = false;
    public ?int $priority = null;

    public function assertValid(): self
    {
        throw_if(
            !$this->hasFiles() && !$this->hasURL(),
            Quit::class,
            'Must have either a valid file or URL'
        );

        return $this;
    }

    public function hasURL(): bool
    {
        return !empty($this->url);
    }

    public function hasFiles(): bool
    {
        return !empty($this->files) && !!$this->files->count();
    }

    public static function pipeline(): DataPipeline
    {
        return parent::pipeline()->through(DataPipes\ExtraFiles::class);
    }

    public static function fillPipeline(): DataPipeline
    {
        return parent::fillPipeline()->through(DataPipes\ExtraFiles::class);
    }
}
