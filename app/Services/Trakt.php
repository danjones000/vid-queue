<?php

namespace App\Services;

use App\Data\WatchData;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Symfony\Component\Console\Output\OutputInterface;

class Trakt
{
    protected $headers = [];

    public function __construct()
    {
        $this->headers = [
            'trakt-api-version' => 2,
            'trakt-api-key'     => env('TRAKT_APP_ID'),
            'Authorization'     => 'Bearer ' . env('TRAKT_OAUTH_TOKEN'),
        ];
    }

    public function request(array $headers = []): PendingRequest
    {
        return Http::withHeaders($this->headers + $headers)->baseUrl('https://api.trakt.tv');
    }

    public function syncHistory(WatchData $data, OutputInterface $output = null): Response
    {
        $output?->writeln(sprintf('Submitting %s to trakt with headers %s', json_encode($data->structuredData), json_encode($this->headers)));

        $resp = $this->request()->post('/sync/history', $data->structuredData);

        $output?->writeln('Response (' . $resp->status() . '):' . PHP_EOL . json_encode($resp->json(), JSON_PRETTY_PRINT));

        return $resp;
    }
}
