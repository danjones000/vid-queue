<?php

namespace App\Queue;

use Illuminate\Queue\Connectors\DatabaseConnector as BaseConnector;

class DatabaseConnector extends BaseConnector
{
    public function connect(array $config)
    {
        return new DatabaseQueue(
            $this->connections->connection($config['connection'] ?? null),
            $config['table'],
            $config['queue'],
            $config['retry_after'] ?? 60,
            $config['after_commit'] ?? null
        );
    }
}
