<?php

namespace App\Commands;

use App\Exceptions\Quit;
use LaravelZero\Framework\Commands\Command as BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class Command extends BaseCommand
{
    public function run(InputInterface $input, OutputInterface $output): int
    {
        try {
            return parent::run($input, $output);
        } catch (Quit $quit) {
            $this->error($quit->getMessage());

            return $quit->getCode() > 0 ? $quit->getCode() : 1;
        }
    }
}
