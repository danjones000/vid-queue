<?php

namespace App\Commands;

use App\Data\WatchData;
use App\Services\Trakt;

class TraktWatchImport extends Command
{
    protected $signature = 'show:watch:import {files* : JSON files to import}';
    protected $description = 'Once online, sync watches from show:watch:export';

    public function handle(Trakt $trakt): int
    {
        $watched = WatchData::from($this->arguments());
        $resp    = $trakt->syncHistory($watched, $this->output);

        return $resp->ok() ? static::SUCCESS : static::FAILURE;
    }
}
