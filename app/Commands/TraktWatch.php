<?php

namespace App\Commands;

use App\Data\WatchFile;
use App\Data\WatchData;
use App\Services\Trakt;

class TraktWatch extends Command
{
    protected $signature = 'show:watch {file : File to watch}';
    protected $description = 'Mark a show as watched right now on Trakt';

    public function handle(Trakt $trakt): int
    {
        $file = WatchFile::from($this->argument('file'));
        $data = WatchData::from(['rawData' => [$file->toArray()]]);
        $resp = $trakt->syncHistory($data, $this->output);

        return $resp->ok() ? static::SUCCESS : static::FAILURE;
    }
}
