<?php

namespace App\Commands;

use App\Data\InputData;
use App\Exceptions\Quit;
use App\Services\ProcessInput;
use Symfony\Component\Console\Output\OutputInterface;

class GetShow extends Command
{
    protected $signature = 'show:get ' .
                         '{--i|input=* : Input files} ' .
                         '{--u|url= : URL to download} ' .
                         '{--d|destination= : Directory of show} ' .
                         '{--t|title= : The title of the episode} ' .
                         '{--p|priority= : Specify priority of job}' .
                         '{--s|sync : Run synchronously}'
                         ;

    protected $description = 'Queue a show to re-encode or download';

    public function handle(ProcessInput $proc): int
    {
        $this->line('Options: ' . json_encode($this->options()), verbosity: OutputInterface::VERBOSITY_DEBUG);
        $input = $this->getInput();
        $this->line('Input: ' . json_encode($input), verbosity: OutputInterface::VERBOSITY_DEBUG);
        $job = $proc->getJob($input);
        $this->line('Job: ' . json_encode($job), verbosity: OutputInterface::VERBOSITY_DEBUG);

        return static::SUCCESS;
    }

    protected function getInput(): InputData
    {
        return InputData::from($this->options())->assertValid();
    }
}
