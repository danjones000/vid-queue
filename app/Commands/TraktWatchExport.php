<?php

namespace App\Commands;

use App\Data\WatchFile;

class TraktWatchExport extends Command
{
    protected $signature = 'show:watch:export {file : File to watch}';
    protected $description = 'Create a json file to mark watched and import with show:watch:import when online';

    public function handle(): int
    {
        $file = WatchFile::from($this->argument('file'));
        $ret = file_put_contents($file->output, $file->toJson());

        if ($ret) {
            $this->line("Saved to {$file->output}");
        } else {
            $this->error('Failed to export');
        }

        return $ret ? static::SUCCESS : static::FAILURE;
    }
}
