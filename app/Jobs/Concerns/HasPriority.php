<?php

namespace App\Jobs\Concerns;

interface HasPriority
{
    public function getPriority(): ?int;
}
